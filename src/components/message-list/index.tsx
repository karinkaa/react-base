import { Box, Container } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import React, { FunctionComponent } from "react";
import { MessageType } from "../../data/message-type";
import { USER_ID } from "../../data/user-data";
import { getDividerText } from "../../helper";
import Message from "../message";
import OwnMessage from "../own-message";

const useStyles = makeStyles({
  divider: {
    marginBottom: 50,
    textAlign: "center",
    width: "100%",
    background: "#9cb0ed",
    height: 20,
    borderRadius: 4
  }
})

interface MessageListProps {
  messages: MessageType[];
  onClickUpdate: (id: string) => void;
  removeMessage: (id: string) => void;
};

const MessageList: FunctionComponent<MessageListProps> = ({ messages, onClickUpdate, removeMessage }) => {
  const classes = useStyles();

  return (
    <Container className={"message-list"}>
      {
        messages.map((message: MessageType, index: number, arr: MessageType[]) => {
          const isOwnMessage = index === 0 || getDividerText(arr[index - 1].createdAt) !== getDividerText(arr[index].createdAt);

          return (
            <React.Fragment key={message.id}>
              {
                isOwnMessage && <Box className={"messages-divider " + classes.divider}>
                  {getDividerText(message.createdAt)}
                </Box>
              }
              {
                (message.userId === USER_ID) ?
                  <OwnMessage key={message.id} message={message} onClickUpdate={onClickUpdate} removeMessage={removeMessage} /> :
                  <Message key={message.id} message={message} />
              }
            </React.Fragment>
          )
        })
      }
    </Container>
  )
};

export default MessageList;
