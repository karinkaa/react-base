import { CircularProgress, makeStyles, Toolbar } from "@material-ui/core";
import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import { MessageType } from "../../data/message-type";
import { createMessage, saveObjectItemTo } from "../../helper";
import Header from "../header";
import MessageInput from "../message-input";
import MessageList from "../message-list";

const useStyles = makeStyles({
  progress: {
    position: "absolute",
    left: "50%",
    top: "50%"
  }
});

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const AlwaysScrollToBottom = () => {
  const elementRef = useRef<HTMLDivElement>();
  useEffect(() => {
    return elementRef?.current?.scrollIntoView();
  });

  // @ts-ignore: Unreachable code error
  return <div ref={elementRef} />;
};

interface ChatProps {
  url: string;
}

const Chat: FunctionComponent<ChatProps> = ({ url }) => {
  const classes = useStyles();
  const [messages, setMessages] = useState<MessageType[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [editMessage, setEditMessage] = useState<MessageType>();

  const onClickUpdate = (id: string): void => {
    const item = messages.find((item: MessageType) => item.id === id);
    if (item) {
      setEditMessage(item);
    }
  }

  const saveMessage = (id: string | undefined, text: string): void => {
    const item = messages.find((item: MessageType) => item.id === id) || createMessage(text);
    let newItem: MessageType;

    if (id) {
      newItem = { ...item, text, editedAt: new Date().toString() };
    } else {
      newItem = { ...item, text };
    }

    setMessages(saveObjectItemTo(messages, newItem));
    setEditMessage(undefined);
  }

  const removeMessage = (id: string): void => {
    const tempMessages = [...messages];
    tempMessages.splice(tempMessages.findIndex((item: MessageType) => item.id === id), 1);

    setMessages(tempMessages);
  }

  useEffect(() => {
    (async function () {
      setIsLoading(true);
      await sleep(1000);

      setMessages(await fetch(url)
        .then(response => response.json())
        .then(data => data)
        .finally(() => {
          setIsLoading(false);
        }));
    }());
  }, []);

  return (
    <div className={"chat"}>
      <Header messages={messages} />
      <Toolbar />
      <Toolbar />
      <div className={classes.progress}>
        {isLoading && <CircularProgress color={"primary"} />}
      </div>
      <MessageList messages={messages} onClickUpdate={onClickUpdate} removeMessage={removeMessage} />
      <Toolbar />
      <MessageInput id={editMessage?.id} text={editMessage?.text} saveMessage={saveMessage} />
      <AlwaysScrollToBottom />
    </div>
  );
}

export default Chat;
