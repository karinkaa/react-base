import { Card, CardContent, CardHeader, IconButton, makeStyles, Typography } from "@material-ui/core";
import { Delete, Update } from "@material-ui/icons";
import React, { FunctionComponent } from "react";
import { MessageType } from "../../data/message-type";
import { formatTime } from "../../helper";

const useStyles = makeStyles({
  root: {
    maxWidth: 600,
    background: "aliceblue",
    marginBottom: 25,
    marginLeft: "auto"
  },
  iconButton: {
    color: "dimgray"
  }
});

interface OwnMessageProps {
  message: MessageType;
  onClickUpdate: (id: string) => void;
  removeMessage: (id: string) => void;
};

const OwnMessage: FunctionComponent<OwnMessageProps> = ({ message, onClickUpdate, removeMessage }) => {
  const classes = useStyles();
  const { id, text, createdAt, editedAt } = message;

  return (
    <Card className={"own-message " + classes.root}>
      <CardHeader
        action={
          <>
            <IconButton
              className={"message-edit " + classes.iconButton}
              onClick={() => onClickUpdate(id)}
            >
              <Update />
            </IconButton>
            <IconButton
              className={"message-delete"}
              onClick={() => removeMessage(id)}
            >
              <Delete />
            </IconButton>
          </>
        }
        subheader={
          <Typography variant={"subtitle2"} className={"message-time"}>
            {formatTime(editedAt || createdAt)} {editedAt && "(edited)"}
          </Typography>
        }
      />
      <CardContent>
        <Typography variant={"body1"} className={"message-text"}>
          {text}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default OwnMessage;
