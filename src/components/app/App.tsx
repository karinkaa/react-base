import { makeStyles } from "@material-ui/core";
import React from "react";
import { MESSAGES } from "../../data/url";
import Chat from "../chat";

const useStyles = makeStyles({
  root: {
    minHeight: "100vh",
    background: "#313537"
  }
});

function App() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Chat url={MESSAGES} />
    </div>
  );
}

export default App;
