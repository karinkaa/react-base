import { Button, Container, makeStyles, TextField } from "@material-ui/core";
import { Check, Send } from "@material-ui/icons";
import React, { FunctionComponent, useEffect, useRef, useState } from "react";

const useStyles = makeStyles({
  root: {
    background: "#313537",
    position: "fixed",
    width: "100%",
    height: 64,
    padding: 0,
    bottom: 0
  },
  text: {
    background: "aliceblue",
    width: "80%",
    maxHeight: 55,
    marginRight: 20,
    borderRadius: 5,
    overflowY: "hidden"
  },
  btn: {
    height: 55,
    width: "10%"
  }
});

interface MessageInputProps {
  id?: string;
  text: string | undefined;
  saveMessage: (id: string | undefined, text: string) => void;
};

const MessageInput: FunctionComponent<MessageInputProps> = ({ id, text, saveMessage }) => {
  const classes = useStyles();
  const ref = useRef<any>();
  const [value, setValue] = useState<string>(text || "");

  useEffect(() => {
    setValue(text || "");
    ref.current.focus();
  }, [text]);

  const handleClick = () => {
    const trimValue = value?.trim();

    if (trimValue) {
      saveMessage(id, trimValue);
      setValue("");
      ref.current.focus();
    }
  }

  return (
    <Container>
      <Container className={"message-input " + classes.root}>
        <TextField
          className={"message-input-text " + classes.text}
          variant={"outlined"}
          value={value}
          inputRef={ref}
          onChange={(e) => setValue(e.target.value)}
          onKeyDown={(e) => e.key === "Enter" && handleClick()}
        />
        <Button
          className={"message-input-button " + classes.btn}
          variant="contained"
          color="primary"
          endIcon={id ? <Check /> : <Send />}
          onClick={handleClick}
        >
          {id ? "Edit" : "Send"}
        </Button>
      </Container>
    </Container>
  );
}

export default MessageInput;
